/* BeamAbort.cpp */
/* Author: Arek Gorzawski */
/* Date:    2021-10-30 */
#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

using namespace std;

long beam_abort(aSubRecord *prec)
{
//    std::cout << "\nABORT Pressed\n";
    *(double *) prec->vala = 0;
    *(double *) prec->valb = 0;
    *(double *) prec->valc = 0;
    epicsOldString* result = (epicsOldString*) prec->vald;
    const char* str_in = "null.csv";
    memcpy((char*)result, str_in, 8);
    return 0;
}

extern "C" {
epicsRegisterFunction(beam_abort);
}