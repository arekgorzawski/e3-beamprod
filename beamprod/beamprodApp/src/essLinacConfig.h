// on: 2023-03-20 20:50:11.439324
// using python script
// auto generated using: https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/beamallowed_commissioning_dtl4.yml?inline=false
// auto generated using: https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/beamcurrent_allowed.yml?inline=false
// auto generated using: https://gitlab.esss.lu.se/icshwi/reftabs/-/raw/master/init/databuffer-ess.yml?inline=false

#include <map>
#include <string>
using namespace std;

struct AllowedModes
{
    string allowed[20];
};

const float absoluteLimitForOLCInuA = 1; //for running hour

struct AllowedParameters
{
    float beamCurrentInmA;
    float pulseLengthInUs;
    float beamFrequency;
    float integratedChargeInUC;
    float averageCurrentInUA;
};

static map<int, string> beamModes = {
    // {EPICS order,  BeamName} as used in mbbo selection combos defined in DB templates
    
    { 0 , "No Beam"},
    { 1 , "Conditioning"},
    { 2 , "Probe"},
    { 3 , "Fast Commissioning"},
    { 4 , "Rf Test"},
    { 5 , "Stability Test"},
    { 6 , "Slow Commissioning"},
    { 7 , "Fast Tuning"},
    { 8 , "Slow Tuning"},
    { 9 , "UNDEFINED"},
    { 10 , "ERROR"},
};

static map<int, string> beamDestinations = {
    // {EPICS order,  BeamDestination} as used in mbbo selection combos defined in DB templates
    
    { 0 , "I Src"},
    { 1 , "LEBT"},
    { 2 , "MEBT"},
    { 3 , "DTL2"},
    { 4 , "DTL4"},
    { 5 , "UNDEFINED"},
    { 6 , "ERROR"},
};

static map<string, AllowedModes> requestTypesMap = {
    // {BeamDestination,  {allowedBeamModes}}
    
    {"I Src", { "Conditioning", }},
    {"LEBT", { "No Beam", "Probe", "Fast Commissioning", "Rf Test", "Stability Test", "Slow Commissioning", "Fast Tuning", "Slow Tuning", }},
    {"MEBT", { "No Beam", "Probe", "Fast Commissioning", "Rf Test", "Slow Commissioning", "Fast Tuning", "Slow Tuning", }},
    {"DTL2", { "No Beam", "Probe", "Fast Commissioning", "Rf Test", "Slow Commissioning", "Fast Tuning", "Slow Tuning", }},
    {"DTL4", { "No Beam", "Probe", "Fast Commissioning", "Rf Test", "Slow Commissioning", "Fast Tuning", "Slow Tuning", }},
};

static map<string, AllowedParameters> requestParametersMap = {
    // {BeamMode,  {pulseLengthInUs, beamCurrentInUA, beamFrequency, integratedChargeInUC, averageCurrentInUA}}
    
    //{"units", { mA, us, Hz, uC, uA, }},
    {"No Beam", { 0, 0, 0, 0, 0, }},
    {"Conditioning", { 0, 0, 0, 0, 0, }},
    {"Probe", { 6, 5, 1, 0.03, 0.03, }},
    {"Fast Commissioning", { 6, 5, 14, 0.03, 0.42, }},
    {"Rf Test", { 6, 50, 1, 0.3, 0.3, }},
    {"Stability Test", { 6, 50, 14, 0.3, 4.2, }},
    {"Slow Commissioning", { 62.5, 5, 1, 0.31, 0.31, }},
    {"Fast Tuning", { 62.5, 5, 14, 0.31, 4.37, }},
    {"Slow Tuning", { 62.5, 50, 1, 3.13, 3.13, }},
    {"Long Pulse Verification", { 62.5, 2860, 0.0333, -1, -1, }},
    {"Shielding Verification", { 0, 0, 0, -1, -1, }},
    {"Production", { 62.5, 2860, 14, -1, -1, }},
};
