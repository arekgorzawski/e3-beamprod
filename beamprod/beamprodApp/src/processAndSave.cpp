/* processAndSaveImpl.cpp */
/* Author: Arek Gorzawski */
/* Date:    2021-05-18 */

#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>

#include "essLinacConfigFunctions.h"
using namespace std;

bool process_and_check(std::vector<std::string> line, std::vector<std::string> header)
{
    //TODO to be implemented
    return true;
}

long process_and_save(aSubRecord *prec)
{

    if (prec->ftc != menuFtypeSTRING)
    {
        errlogSevPrintf(errlogMajor, "%s incorrect input argument type A", prec->c);
        return 1;
    }

    const long _headerL = atol((const char*)(prec->c));
    const long _linesCount = atol((const char*)(prec->d));
    std::string _file_name((const char*)(prec->e));
    std::string _beam_mode((const char*)(prec->f));

    if (_headerL == 0 || _linesCount == 0){
        errlogSevPrintf(errlogMajor, "Incorrect header length or number of lines! Aborting.");
        return 1;
    }

    std::vector<std::string> _dataInStringArray;
    for (unsigned int i = 0; i < prec->noa; ++i) {
        std::string s(((epicsOldString*)(prec->a))[i], sizeof(epicsOldString));
        _dataInStringArray.push_back(s);
    }

    std::vector<std::string> _header;
    for (unsigned int i = 0; i < prec->nob; ++i) {
        std::string s(((epicsOldString*)(prec->b))[i], sizeof(epicsOldString));
        if (i >= _headerL) break;
        _header.push_back(s);
    }

    int elementCounter = 0;
    int lineCounter = 1;
    bool linesCheckResult = true;
    std::vector<std::string> oneLine;
    for (auto i = _dataInStringArray.begin(); i != _dataInStringArray.end(); i++)
    {
        if (lineCounter > _linesCount) break;
        if (elementCounter % _headerL == 0 && elementCounter > 0)
        {
        // TODO check timings and payload (derive beam Mode/real frequency)
        // TODO find and add the real frequency of the table report wrong if different than requested
            linesCheckResult & process_and_check(oneLine, _header);
            lineCounter++;
        }
        oneLine.push_back(*i);
        elementCounter++;
    }

    if (linesCheckResult)
    {
        std::vector<std::vector<std::string>> ttToSave;
        ttToSave.push_back(_header);
        elementCounter = 0;
        lineCounter = 1;
        oneLine.clear();
        for (auto i = _dataInStringArray.begin(); i != _dataInStringArray.end(); i++)
        {
            if (lineCounter > _linesCount) break;
            if (elementCounter % _headerL == 0 && elementCounter > 0){
                ttToSave.push_back(oneLine);
                lineCounter++;
                oneLine.clear();
            }
            oneLine.push_back(*i);
            elementCounter++;
        }
        writeTTToCSVFile(_file_name, ttToSave);
    }

    epicsOldString* result = (epicsOldString*) prec->vala;
    const char* str_in = "Import success!";
    memcpy((char*)result, str_in, 15);

    return 0; /* process output links */
}

extern "C"
{
    epicsRegisterFunction(process_and_save);
}