/**
 * @file timingtables.cpp
 * @author Arek Gorzawski (arek.gorzawski@ess.eu)
 * @brief
 * @version 0.1
 * @date 2022-02-01
 *
 * @copyright Copyright (c) 2022 European Spallation Source ERIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <iostream>
#include <string>
#include <vector>
#include <utility>

#include <pv/pvDatabase.h>
#include <pv/channelProviderLocal.h>

#include	<dbStaticLib.h>
#include	<dbAccess.h>	    /* includes dbDefs.h, dbBase.h, dbAddr.h, dbFldTypes.h */
#include	<recSup.h>		    /* rset */
#include	<dbConvert.h> 	    /* dbPutConvertRoutine */
#include	<dbConvertFast.h>	/* dbFastPutConvertRoutine */
#include	<initHooks.h>
#include	<epicsThread.h>
#include	<errlog.h>
#include	<iocsh.h>
#include	<special.h>
#include	<macLib.h>
#include	<epicsString.h>
#include	<dbAccessDefs.h>
#include	<epicsStdio.h>

#include <iocsh.h>
#include <initHooks.h>

// The following must be the last include for code database uses
#include <epicsExport.h>

#define epicsExportSharedSymbols
#include "timingtable.h"

using namespace std;
using namespace epics::pvData;
using namespace epics::pvDatabase;


/* Static variables that capture the state of this feature */
vector<pair<string, string> > inputTable;
bool configureFunctionCalled = false;


void timingTablesHooks(initHookState state)
{
    if(state==initHookAfterIocRunning) {
        if (inputTable.size() > 0) {
            for (size_t i = 0; i < inputTable.size(); i++) {
                /* Create record with NTTable */
                string ntTableRecordName = inputTable[i].second;
                TimingTableRecordPtr record = TimingTableRecord::create(ntTableRecordName);
                record->updateTable();
                bool result = PVDatabase::getMaster()->addRecord(record);
                if(!result)
                    errlogPrintf("iocMetadataConfigure: NTTable record %s was not added to the IOC\n", ntTableRecordName.c_str());
            }
        }
        configureFunctionCalled = true;
    }
}

/***********************************************************************/

static const iocshArg funcArg0 = { "Unused", iocshArgString };
static const iocshArg funcArg1 = { "NTTable PV Name", iocshArgString };
static const iocshArg *funcArgs[] = {&funcArg0, &funcArg1};
static const iocshFuncDef timingTablesCollectorFuncDef = {"iocTimingTableConfigure", 2, funcArgs};

static void timingTablesCollector(const iocshArgBuf *args)
{
    if (!configureFunctionCalled) {
        if(!args[0].sval || !args[1].sval) {
            throw std::runtime_error("iocTimingTableConfigure: missing arguments. Usage: iocMetadataConfigure [INFOTAG_STRING] [NTTABLE_NAME]");
        }
        /* Sets the matching string and PV name according to the arguments passed */
        pair<string, string> tmp(args[0].sval, args[1].sval);
        inputTable.push_back(tmp);
    }
    else
    {
        errlogPrintf("iocTimingTableConfigure: IOC already running, this function call causes no effect\n");
    }
}

static void timingTablesCollectorRegister(void)
{
    static int firstTime = 1;
    if (firstTime) {
        configureFunctionCalled = false;
        iocshRegister(&timingTablesCollectorFuncDef, timingTablesCollector);
        initHookRegister(&timingTablesHooks);
    }
}

extern "C" {
    epicsExportRegistrar(timingTablesCollectorRegister);
}
