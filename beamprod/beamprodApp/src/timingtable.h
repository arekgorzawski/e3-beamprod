/**
 * @file timingtable.h
 * @author Arek Gorzawski (arek.gorzawski@ess.eu)
 * @brief
 * @version 0.1
 * @date 2021-11-300
 *
 * @copyright Copyright (c) 2020 European Spallation Source ERIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#ifndef _TIMING_TABLE_H_
#define _TIMING_TABLE_H_

#include <vector>
#include <string>

#include <pv/pvDatabase.h>
#include <pv/timeStamp.h>
#include <pv/pvTimeStamp.h>

#include <shareLib.h>

class TimingTableRecord;
typedef std::tr1::shared_ptr<TimingTableRecord> TimingTableRecordPtr;
typedef std::vector<std::vector<std::string> > TimingTableRecordType;


class epicsShareClass TimingTableRecord :
    public epics::pvDatabase::PVRecord
{
public:
    POINTER_DEFINITIONS(TimingTableRecord);
    static TimingTableRecordPtr create(std::string const & recordName);
    virtual void process();
    virtual ~TimingTableRecord() {}
    virtual bool init() {return false;}
    TimingTableRecordType& getTableRef() {return rawData; }
    void updateTable();

private:
    TimingTableRecord(std::string const & recordName,
        epics::pvData::PVStructurePtr const & pvStructure);

    TimingTableRecordType rawData;
};

#endif  /* _TIMING_TABLE_H_ */
