/**
 * @file timingtable.cpp
 * @author Arek Gorzawski (arek.gorzawski@ess.eu)
 * @brief
 * @version 0.1
 * @date 2021-11-30
 *
 * @copyright Copyright (c) 2022 European Spallation Source ERIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <pv/pvDatabase.h>
#include <pv/standardField.h>

#define epicsExportSharedSymbols
#include "timingtable.h"

using namespace epics::pvData;
using namespace epics::pvDatabase;
namespace pvd = epics::pvData;
using std::tr1::static_pointer_cast;
using std::string;
using std::vector;

TimingTableRecordPtr TimingTableRecord::create(string const & recordName)
{
    StandardFieldPtr standardField = getStandardField();
    FieldCreatePtr fieldCreate = getFieldCreate();
    PVDataCreatePtr pvDataCreate = getPVDataCreate();

    StructureConstPtr topStructure(getFieldCreate()->createFieldBuilder()
                                ->setId("epics:nt/NTTable:1.0")
                                ->addNestedStructure("MetaData")
                                    ->add("BeamMode", pvd::pvString)
                                    ->add("BLenSrc", pvd::pvFloat)
                                    ->add("BLenLebt", pvd::pvFloat)
                                    ->add("BLenMebt", pvd::pvFloat)
                                    ->add("PulseFrequency", pvd::pvFloat)
                                    ->add("Comment", pvd::pvString)
                                    ->addArray("Keywords", pvd::pvString)
                                ->endNested()
                                ->addArray("CycleHeader", pvd::pvString)
                                ->addArray("DataBufferHeader", pvd::pvString)
                                ->addNestedStructureArray("SuperCycle")
                                    ->addArray("Cycle", pvd::pvFloat)
                                    ->addArray("DataBuffer", pvd::pvFloat)
                                ->endNested()
                                ->createStructure());

    PVStructurePtr pvStructure = pvDataCreate->createPVStructure(topStructure);
    TimingTableRecordPtr pvRecord(new TimingTableRecord(recordName, pvStructure));
    pvRecord->initPVRecord();
    return pvRecord;
}

TimingTableRecord::TimingTableRecord(
    string const & recordName,
    PVStructurePtr const & pvStructure)
: PVRecord(recordName,pvStructure)
{}

void TimingTableRecord::updateTable() {
    // FIXME any default value?
}

void TimingTableRecord::process()
{
    // prevent any external "put" operation to change the value of the record
    // updateTable();
}
