/* olcImpl.cpp */
/* Author: Arek Gorzawski */
/* Date:    2021-06-01 */

#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>
#include <sstream>
#include <iostream>

#include "essLinacConfigFunctions.h"
#include "circularBuffer.h"

using namespace std;

static CircularBuffer<float, BUFFER_SIZE_60_MIN> _time60min;
static CircularBuffer<float, BUFFER_SIZE_60_MIN> _data60min;
static CircularBuffer<float, BUFFER_SIZE_60_MIN> _measData60min;

long recalculate_integrated_charge(aSubRecord *prec)
{
    const char* beamMode = (const char*)(prec->a);
    const char* beamDest = (const char*)(prec->b);
    const char* beamState = (const char*)(prec->c);
    double pulseChargeInUC = *(epicsFloat64*)(prec->d);
    float repRate = *(epicsFloat64*)(prec->e);
    float lastSecondIntendedIntegratedBeamInUA = 0.0;
    float beamTime = 0.0;

    float destinationCorrection = 0.0;
    // This is ONLY valid for DTL4 destination for SRR3 commissioning
    if ( strcmp(beamDest, "DTL4") == 0 ){
        destinationCorrection = 1.0;
    }
    // This is ONLY counting when the Beam ON
    if ( strcmp(beamState, "On") == 0 ){
        beamTime = repRate * requestParametersMap[beamMode].pulseLengthInUs * 1e-6;
        lastSecondIntendedIntegratedBeamInUA = destinationCorrection * beamTime * requestParametersMap[beamMode].beamCurrentInmA * 1e3;
    }

    _data60min.write(&lastSecondIntendedIntegratedBeamInUA, 1);
    float lastSecondMeasuredInUA = destinationCorrection * pulseChargeInUC *  repRate;
    _measData60min.write(&lastSecondMeasuredInUA, 1);
    _time60min.write(&beamTime, 1);
    *(double *) prec->vala = _time60min.sum();
    *(double *) prec->valb = _data60min.avg();
    float max = 100.0;
    *(double *) prec->valc = (_data60min.avg() / absoluteLimitForOLCInuA) * max;
    *(double *) prec->vald = _measData60min.avg();
    *(double *) prec->vale = (_measData60min.avg() / absoluteLimitForOLCInuA) * max;
    return 0;
}

extern "C" {
epicsRegisterFunction(recalculate_integrated_charge);
}