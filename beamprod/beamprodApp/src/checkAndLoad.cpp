/**
 * @file checkIfAllowedImpl.cpp
 * @author Arek Gorzawski
 * @date    2022-01-31
 *
 * @copyright Copyright (c) 2022 European Spallation Source ERIC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <string.h>
#include <stdlib.h>
#include <registryFunction.h>
#include <aSubRecord.h>
#include <menuFtype.h>
#include <errlog.h>
#include <epicsString.h>
#include <epicsExport.h>

#include <string>
#include <vector>

#include "timingtable.h"
#include "essLinacConfigFunctions.h"

#include <pv/pvDatabase.h>
#include <pv/standardField.h>
#define epicsExportSharedSymbols
#include <dbAccessDefs.h>

#define VALID_CYCLE_HEADER_MARKER "BPulseSt"
#define NOMINAL_NUMBER_OF_CYCLES 14
#define NULL_TABLE "null.csv"

using namespace std;
using namespace epics::pvData;
using namespace epics::pvDatabase;

PVFloatArray::svector makeProperCopy(shared_vector<float> vector);

struct MetaDataStub
{
    string beamMode;
    float detectedFrequency;
    float beamCurrentInmA;
    float detectedPLSource;
    float detectedPLLEBT;
    float detectedPLMEBT;
    float pulseL;
    string comment;
};

static void updateRecord(PVRecordPtr outPVPtr,
                         MetaDataStub metaDataStub,
                         shared_vector<string> headersArray, shared_vector<string> dataBufferArray,
                         std::vector<shared_vector<float> > cycles,
                         std::vector<shared_vector<float> > dataBuffers);

long check_and_load_table(aSubRecord *prec)
{
    epicsOldString* result = (epicsOldString*) prec->vale;
    const char* str_in = "Started Loading...";
    memcpy((char*)result, str_in, 18);

    const char* timingTableName = (const char*)(prec->a);
    std::string ttstr(timingTableName);
    const char* skipCheck = (const char*)(prec->b);
    long skipCheckTT = strtol(skipCheck, NULL, 0);
    const char* beamModeC = (const char*)(prec->c);
    std::string beamMode(beamModeC);
    const char* beamDestinationC = (const char*)(prec->d);
    std::string beamDestination(beamDestinationC);
    PVRecordPtr outPVPtr = PVDatabase::getMaster()->findRecord("BPROD:Ops:TTLoaded-RB");
    //TODO Find a way to remove the hardcoded PV, take from INPUT/OUTPUT?
    MetaDataStub metaDataStub = {"n/a", 0, 0, -1, -1, -1, -1, ttstr};
    shared_vector<string> headersArray;
    shared_vector<string> dataBufferArray;
    std::vector<shared_vector<float> > cycles;
    std::vector<shared_vector<float> > dataBuffers;
    if (ttstr.compare(NULL_TABLE) == 0 || !checkIfFileExists(ttstr))
    {
        // Populate NTTable Record with empty state
        updateRecord(outPVPtr, metaDataStub, headersArray, dataBufferArray, cycles, dataBuffers);
        return 1;
    }

    // ==============================================================================
    // === LOAD from FILE (name provided via prec->a
    std::vector<std::vector<std::string>> parsedCsv = readTTFromCSVFile(ttstr);

    // ==============================================================================
    // === CHECK WRT TO THE BEAM MODE & internal ===
    // decode the file name details
    std::vector<string> fileDetails = tokenize(ttstr, "__");
    if (fileDetails.size() > 1)
    {
        std::vector<string> details;
        if (fileDetails.size() > 2)  // Temporary solution for the backwards compatibility with file names
        {
            details = tokenize(fileDetails[2], "_");
        } else
        {
            details = tokenize(fileDetails[1], "_");
        }
       // metaDataStub.beamMode = fileDetails[0];
        if (details.size() > 2 )
        {
            metaDataStub.detectedPLSource = strtof(details[0].c_str(), NULL);
            metaDataStub.detectedPLLEBT = strtof(details[1].c_str(), NULL);
            metaDataStub.detectedPLMEBT = strtof(details[2].c_str(), NULL);
        }
    }
    metaDataStub.pulseL = metaDataStub.detectedPLMEBT;
    // ==============================================================================
    // === UPDATE THE PVs related to the pulse length
    *(double *) prec->vala = metaDataStub.detectedPLSource;
    *(double *) prec->valb = metaDataStub.detectedPLLEBT;
    *(double *) prec->valc = metaDataStub.pulseL;
    str_in = "Table loaded...";
    memcpy((char*)result, str_in, 18);
    //shared_vector<string> keywords;
    int indexForValidCycleMarker = -1;
    int i = 0;
    map<int, string> indicesForDataBuffer;
    for (auto header: parsedCsv.front())
    {
        // std::cout <<" header " << i << " \n ";
        if (header == VALID_CYCLE_HEADER_MARKER)
            indexForValidCycleMarker = i;
        if (header != "")
        {
            //TODO get better handling here maybe external PVRecord with array of buffer definiton
            // also with VALID_CYCLE_HEADER_MARKER defined above.

            if (header != "BLen" && header !="BEn" && header != "BCurr" )
            {
                headersArray.push_back(header);
            } else {
                dataBufferArray.push_back(header);
                indicesForDataBuffer.insert(pair<int, string>(i, header));
            }
        }
        i++;
    }
    int csvRowIndex = 0;
    std::vector<int> rowIndicesForValidCycleMarker;
    for (auto row : parsedCsv)
    {
        if (csvRowIndex == 0) { //header
            csvRowIndex++;
            continue;
        }
        shared_vector<float> tmpcycle;
        shared_vector<float> tmpbuffer;
        int columnIndex = 0;
        std::cout << "Loaded row "<< csvRowIndex << " \n";
        for (auto cell : row)
        {
            if (columnIndex >= headersArray.size()+dataBufferArray.size())
            {
                break;
            }
            if (cell != ""  && cell != " " && cell !="\n") // FIXME THAT add trim?
            {
                if (indicesForDataBuffer.count(columnIndex))
                {
                    tmpbuffer.push_back(std::stof(cell));
                } else {
                    tmpcycle.push_back(std::stof(cell));
                }
                if (columnIndex == indexForValidCycleMarker)
                {
                    rowIndicesForValidCycleMarker.push_back(csvRowIndex);
                }
            }
            else {

                tmpcycle.push_back(DEFAULT_EMPTY_TIMING);
            }
            columnIndex++;
        }
        cycles.push_back(tmpcycle);
        dataBuffers.push_back(tmpbuffer);
        csvRowIndex++;
    }

    metaDataStub.detectedFrequency = (float) NOMINAL_NUMBER_OF_CYCLES  * (float) rowIndicesForValidCycleMarker.size() / (float) cycles.size();
    std::cout << " freq " << std::fixed << std::setprecision(1) << metaDataStub.detectedFrequency;
    std::cout   << ", N="<<NOMINAL_NUMBER_OF_CYCLES<< ", valid rows=" << rowIndicesForValidCycleMarker.size() << ", cycles=" <<cycles.size() << "\n";
    *(double *) prec->vald = metaDataStub.detectedFrequency;
    *(double *) prec->valf = 1;
    const char *destNotForTheTableCheckC = "LEBT";
    if (strcmp(beamDestinationC, destNotForTheTableCheckC) != 0)
    {
        *(double *) prec->valf = check_pulse_length_for_mode(beamMode, metaDataStub.pulseL) *
                                 check_frequency_for_mode (beamMode,metaDataStub.detectedFrequency);
    }
    if (rowIndicesForValidCycleMarker.size() > 1)
    {
        //FIXME add min value between cycleLines check, for now ONLY absolute number of puslses is checked
    }
    bool checkResult = false;
    // check if the table is OK for the selected beam mode
    if (
        (check_frequency_for_mode(beamMode, metaDataStub.detectedFrequency) *
        check_pulse_length_for_mode(beamMode, metaDataStub.pulseL) *
        check_beam_current_for_mode(beamMode, metaDataStub.beamCurrentInmA) > 0))
    {
        str_in = "[OK] Freq&Current&BeamL&Mode";
        memcpy((char*)result, str_in, 28);
        std::cout << beamMode << " that is selected is COMPATIBLE with " << metaDataStub.detectedFrequency <<"Hz \n";
        // TODO move here (from the Fanout record) the actual table load in the SCE
        checkResult = true;
    }
    else
    {
        str_in = "[NOK] Freq&Current&BeamL&Mod";
        memcpy((char*)result, str_in, 28);
        std::cout << beamMode << " that is selected is NOT compatible with " << metaDataStub.detectedFrequency <<"Hz \n";
        //FIXME introduce proper log
    }

    // FIXME activate break and fixt the read of the check PV
    if (!checkResult && skipCheckTT  < 1 ){
        return 0;
    }
    // ==============================================================================
    // Populate NTTable Record
    // ==============================================================================
    //FIXME move to timingtable.cpp?
    //FIXME cannot use the following function here, there might be some problem with freeze() inside the function
    // updateRecord(outPVPtr, metaDataStub, headersArray, dataBufferArray, cycles, dataBuffers);
    PVStructurePtr pvStructure = outPVPtr->getPVStructure();
    PVStringArrayPtr cycleLabels = pvStructure->getSubField<PVStringArray>("CycleHeader");
    cycleLabels->putFrom(freeze(headersArray));
    PVStringArrayPtr dbLabels = pvStructure->getSubField<PVStringArray>("DataBufferHeader");
    dbLabels->putFrom(freeze(dataBufferArray));
    PVStringPtr mdBMode = pvStructure->getSubField<PVString>("MetaData.BeamMode");
    mdBMode->put(beamMode);
//    PVStringArrayPtr keywordsOut = pvStructure->getSubField<PVStringArray>("Keywords");
//    keywordsOut->putFrom(freeze(keywords));
    PVStringPtr mdComment = pvStructure->getSubField<PVString>("MetaData.Comment");
    mdComment->put(metaDataStub.comment);
    PVFloatPtr mdPF = pvStructure->getSubField<PVFloat>("MetaData.PulseFrequency");
    mdPF->put(metaDataStub.detectedFrequency);
    PVFloatPtr mdPLS = pvStructure->getSubField<PVFloat>("MetaData.BLenSrc");
    mdPLS->put(metaDataStub.detectedPLSource);
    PVFloatPtr mdPLLEBT = pvStructure->getSubField<PVFloat>("MetaData.BLenLebt");
    mdPLLEBT->put(metaDataStub.detectedPLLEBT);
    PVFloatPtr mdPLMEBT = pvStructure->getSubField<PVFloat>("MetaData.BLenMebt");
    mdPLMEBT->put(metaDataStub.detectedPLMEBT);
    PVStructureArrayPtr superCyclePtr = pvStructure->getSubField<PVStructureArray>("SuperCycle");
    size_t len = cycles.size();
    PVStructureArray::svector superCycleStructures(len);
    int cycleIndex = 0;
    for (shared_vector<float> cycle: cycles)
    {
        superCycleStructures[cycleIndex] = getPVDataCreate()->createPVStructure(
                            superCyclePtr->getStructureArray()->getStructure());
        PVStructurePtr superCycleStructurePtr = superCycleStructures[cycleIndex];
        PVFloatArrayPtr cycleArrayPtr = superCycleStructurePtr->getSubField<PVFloatArray>("Cycle");
        PVFloatArray::svector data = makeProperCopy(cycle); //TODO check why one cannot use shared_vector<float>
        cycleArrayPtr->replace(freeze(data));
        PVFloatArrayPtr dataBufferPtr = superCycleStructurePtr->getSubField<PVFloatArray>("DataBuffer");
        PVFloatArray::svector dataB = makeProperCopy(dataBuffers[cycleIndex]);
        dataBufferPtr->replace(freeze(dataB));
        cycleIndex++;
    }
    superCyclePtr->replace(freeze(superCycleStructures));
    return 0;
}

static void updateRecord(PVRecordPtr outPVPtr,
                         MetaDataStub metaDataStub,
                         shared_vector<string> headersArray,
                         shared_vector<string> dataBufferArray,
                         std::vector<shared_vector<float>> cycles,
                         std::vector<shared_vector<float>> dataBuffers)
{
    //FIXME move to timingtable.cpp?
    PVStructurePtr pvStructure = outPVPtr->getPVStructure();
    PVStringArrayPtr cycleLabels = pvStructure->getSubField<PVStringArray>("CycleHeader");
    cycleLabels->putFrom(freeze(headersArray));
    PVStringArrayPtr dbLabels = pvStructure->getSubField<PVStringArray>("DataBufferHeader");
    dbLabels->putFrom(freeze(dataBufferArray));
    PVStringPtr mdBMode = pvStructure->getSubField<PVString>("MetaData.BeamMode");
    mdBMode->put(metaDataStub.beamMode);
//    PVStringArrayPtr keywordsOut = pvStructure->getSubField<PVStringArray>("Keywords");
//    keywordsOut->putFrom(freeze(keywords));
    PVStringPtr mdComment = pvStructure->getSubField<PVString>("MetaData.Comment");
    mdComment->put(metaDataStub.comment);
    PVFloatPtr mdPF = pvStructure->getSubField<PVFloat>("MetaData.PulseFrequency");
    mdPF->put(metaDataStub.detectedFrequency);
    PVFloatPtr mdPLS = pvStructure->getSubField<PVFloat>("MetaData.BLenSrc");
    mdPLS->put(metaDataStub.detectedPLSource);
    PVFloatPtr mdPLLEBT = pvStructure->getSubField<PVFloat>("MetaData.BLenLebt");
    mdPLLEBT->put(metaDataStub.detectedPLLEBT);
    PVFloatPtr mdPLMEBT = pvStructure->getSubField<PVFloat>("MetaData.BLenMebt");
    mdPLMEBT->put(metaDataStub.detectedPLMEBT);

    PVStructureArrayPtr superCyclePtr = pvStructure->getSubField<PVStructureArray>("SuperCycle");
    size_t len = cycles.size();
    PVStructureArray::svector superCycleStructures(len);
    int cycleIndex = 0;
    for (shared_vector<float> cycle: cycles)
    {
        superCycleStructures[cycleIndex] = getPVDataCreate()->createPVStructure(
                            superCyclePtr->getStructureArray()->getStructure());
        PVStructurePtr superCycleStructurePtr = superCycleStructures[cycleIndex];
        PVFloatArrayPtr cycleArrayPtr = superCycleStructurePtr->getSubField<PVFloatArray>("Cycle");
        PVFloatArray::svector data = makeProperCopy(cycle); //TODO check why one cannot use shared_vector<float>
        cycleArrayPtr->replace(freeze(data));
        PVFloatArrayPtr dataBufferPtr = superCycleStructurePtr->getSubField<PVFloatArray>("DataBuffer");
        PVFloatArray::svector dataB = makeProperCopy(dataBuffers[cycleIndex]);
        dataBufferPtr->replace(freeze(dataB));
        cycleIndex++;
    }
    superCyclePtr->replace(freeze(superCycleStructures));
}


PVFloatArray::svector makeProperCopy(shared_vector<float> vector)
{
    PVFloatArray::svector x(vector.size());
    int i=0;
    for (float one: vector){
        x[i] = one;
        i++;
    }
    return x;
}

extern "C" {
epicsRegisterFunction(check_and_load_table);
}