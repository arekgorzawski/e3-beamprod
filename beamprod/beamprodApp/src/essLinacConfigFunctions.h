/* Author: Arek Gorzawski */
/* Date:    2021-03-18 */
#include <map>
#include <string>
#include <algorithm>

#include <sstream>
#include <iostream>
#include <fstream>

using namespace std;

#define DELIMITER ","
#define ENDLINE "\n"
#define SAVEFOLDER "/opt/timingtabs/"
#define FILENAMEDELIMIT "__"
#define DEFAULT_EMPTY_TIMING -1.0
#define FILEFORMAT ".csv"

#include "essLinacConfig.h"

static int check_pulse_length_for_mode(string beamMode, float pulseL)
{
    // TODO add map check key
    if (pulseL <= requestParametersMap[beamMode].pulseLengthInUs)
        return 1;
    return 0;
}

static int check_beam_current_for_mode(string beamMode, float current)
{
    // TODO add map check key
    if (current <= requestParametersMap[beamMode].beamCurrentInmA)
        return 1;
    return 0;
}

static int check_frequency_for_mode(string beamMode, float frequency)
{
    // TODO add map check key
    if (frequency <= requestParametersMap[beamMode].beamFrequency)
        return 1;
    return 0;
}


static int check_allowed_mode_for_destination(string beamMode, string beamDestination)
{
    string *foo = std::find(std::begin(requestTypesMap[beamDestination].allowed), std::end(requestTypesMap[beamDestination].allowed), beamMode);
    if (foo != std::end(requestTypesMap[beamDestination].allowed)) {
        return 1;
    } else {
        return 0;
    }
    return -1;
}

static std::string prepare_CSV_Line(std::vector<std::string> line)
{
    std::stringstream ss;
    for (auto e: line)
    {
        ss << e.c_str() << DELIMITER;
    }
    ss << ENDLINE;
    return ss.str();
}

static bool checkIfFileExists(string name)
{
    std::string fullFileName = SAVEFOLDER + name;
    std::ifstream f(fullFileName);
    return f.good();
}

static std::vector<std::vector<std::string>> readTTFromCSVFile(string fileName)
{
    std::string fullFileName = SAVEFOLDER + fileName;
    std::ifstream  ttRawData(fullFileName);
    std::string line;
    std::vector<std::vector<std::string> > parsedCsv;
    while(std::getline(ttRawData, line))
    {
        std::stringstream lineStream(line);
        std::string cell;
        std::vector<std::string> parsedRow;
        while(std::getline(lineStream,cell,','))
        {
            parsedRow.push_back(cell);
        }
        parsedCsv.push_back(parsedRow);
    }
    return parsedCsv;
}

static void writeTTToCSVFile(string fileName, std::vector<std::vector<std::string>> cycles)
{
        // PROCEED With files saving
        std::string fullFileName = SAVEFOLDER + fileName;
        std::ofstream outFile(fullFileName);
        for (auto oneLine: cycles)
        {
            outFile << prepare_CSV_Line(oneLine);
        }
        outFile.close();
}

static std::vector<string> tokenize(string s, string del = "_")
{
    std::vector<string> tokens;
    int start = 0;
    int end = s.find(del);
    while (end != -1) {
        std::string tmp(s.substr(start, end - start));
        tokens.push_back(tmp);
        start = end + del.size();
        end = s.find(del, start);
    }
    std::string tmp(s.substr(start, end - start));
    tokens.push_back(tmp);
    return tokens;
}