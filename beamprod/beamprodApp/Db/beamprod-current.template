#! ==============================================================================
#! Basic Beam Production PVs related to the BeamCurrent Setup
#! ==============================================================================

record(ai, "$(P):$(R):Curr-SP") {
    field(DESC, "Desired post-IRIS current")
    field(VAL, "5")
    field(PINI, "YES")
    field(EGU, "mA")
    field(FLNK, "$(P):$(R):MoveIris")
}

record(waveform, "$(P):$(R):IrisToCurr-SP") {
    field(DESC, "Mapping of IRIS to beam current")
    field(FTVL, "DOUBLE")
    field(NELM, "76")
    field(EGU, "mA")
    field(PINI, "YES")
    #! waveform transfer function, step of 1mm (index) value of the recorded current.
    #! default mapping
    field(INP, [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,
                20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,
                40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,
                60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76])

}

record(ao, "$(P):$(R):AllowedBModeCurrent") {
    field(PINI, "YES")
    field(DESC, "BeamCurrent Mode compatibility")
    field(VAL, "1")
}

record(aSub, "$(P):$(R):MoveIris"){
    field(SNAM, "set_iris_for_current")
    field(PINI, "YES")
    field(DESC, "Iris adjustment wrt calibration waveform")

    field(FTA, "DOUBLE")
    field(INPA, "$(P):$(R):Curr-SP")

    field(FTB, "DOUBLE")
    field(NOB, "76") #!
    field(NEB, "76") #!
    field(INPB, "$(P):$(R):IrisToCurr-SP")

    field(FTC, "STRING")
    field(INPC, "$(P):$(R):BMode")

    field(OUTA, "$(LEBT_IRIS):Aperture.VAL PP")
    field(FTVA, "DOUBLE")
    field(OUTB, "$(P):$(R):AllowedBModeCurrent PP")
    field(FTVB, "DOUBLE")
}
