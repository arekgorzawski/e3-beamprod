# This should be a test or example startup script
require beamprod

iocshLoad("$(beamprod_DIR)/beamprod.iocsh", "P=BPROD, R=Ops, PP=TD-M:Ctrl-SCE-1, LEBT_IRIS=LEBT-Iris:ID-Iris-01, MAGNETRON=ISrc-CS:ISS-Magtr-01, MEBT_CHP=MEBT-010:BMD-Chop-001, BCM_CRATE=PBI-BCM01:Ctrl-AMC-210")
