# BeamProduction IOC change log

## 0.8.0
- Exposed Beam Mode/ Timing Table compatibility
- Exposed mechanism to control the beam current via IRIS relevant transfer map
- Exposed Beam Destination constraint for the SRR3 MEBT FC
- 

## 0.7.1
- Bug fixes (null tables/ wrong tables names/ nonexisting files)
- Exposed PV for controlling BeamModeChecks before the swap (ONLY BeamProduction)
- Fixed the OLC input (ACCOP-244)

## 0.7.0
- BeamInhibit / ResetInterlocks / BeamStart exposed (ACCOP-199 and ACCOP-242)
- Timing table swap with BeamON (crosscheck on the beam mode! ACCOP-219) 
- Timing table exposed as NTTable (only readback) ACCOP-195 ACCOP-188

## 0.6.2
- moved into e3/wrappers namespace and added CI pipeline

## 0.6.1
- fix for the EPICS null characters in the saved file

## 0.6.0
- simplified the file add (only selected file needed, name preserved)
- beam abort (several PV set to various states) logic from OPI to IOC

## 0.5.2
- removed BCM support (ACCOP-172)

## 0.5.1
- included docs with exposed pv list (docs/beamproduction.pvs)
- bug fix AllowedBeamModesParameters regenerated - typo in the reftab)

## 0.5.0
- BCM support for the BeamMode dependant setup
- included the BCM charge per pulse to calculate the current for the OLC limits

## 0.4.2
- Added PVs to expose the measured OLC limits calculations,
- Added *mockup* PV link to the BCM measurement.
 
## 0.4.1
- Fix: the limits and beam current budget used is calculated wrt to the absolute limit (1uA running hour, ESS-0357253).

## 0.4.0
- Including the support for the Operational Limits and Conditions (OLC) ESS-0357253,
- Exposing the limits as PVs as a function of selected BeamMode, 
  - `:BParamsIntCharge` integrated charge,
  - `:BParamsAvgCurr` average beam current,
- Calculating and exposing the running sum for: 
  - a minute, 12 minutes, and an hour for the **intended maximum beam current** (i.e. for selected mode: freq x pulse length x beam current)
  - tbc

## 0.3.0
- Python generator for the C++ code used for the `aSub` EPICS records, based on the 
  input available in `reftabs`
  - C++  static code is committed in the repository.
- `aSub` record for the combination validity of the `BMode` and `BDestination`
  (triggered when either of them is changed) `:AllowedBModeBDst` 
- `aSub` record for the  maximum beam parameters for the selected `:BMode` 
  (triggered when `:BMode` is changed)
  - `:BParamsPL` beam pulse length,
  - `:BParamsI` beam current,
  - `:BParamsF` beam frequency.
- `aSub` record `:TTAdd` for performing and import of the new TimingTable 
  triggered on setting `:TTData`. Please note, that if `:TTMetaNbLines` and `:TTMetaHeaderL` are zero, no processing will be performed. The following PVs are taken into account 
  (and should be set prior to the `:TTData`) when processing `:TTAdd`:
  - `:TTMetaNbLines` number of lines, default:0,
  - `:TTMetaHeaderL` header length, default:0,
  - `:TTMetaHeader` header array (strings),
  - `:TTMetaFileName` desired filename comment, default:filename_comment,
  - `:TTMetaBeamMode` table target timing table, default:TEST,
- Exposing PV with the software version `:Ver`

## 0.2.1
- explicit EPICS `PROC` and `CA` flags for the forwarded fields `:BMode` and `:BDestination` 
  in order to trigger the process of the linked fields in the other IOC.

## 0.2.0
- cleanup for the new e3 template

## 0.1.1
- convention change (following the TD-M) `PB` to `B` for all beam related PVs
- disable of all WIP records (as they were giving many errors in the IOC log)

## 0.1.0
- Initial project import/ideas, setting the IOC prefix as `BPROD:Ops:`
- Basic PVs:
  - Beam mode/destination:
  - `:BMode`, internally a forward link to `{SCE}:BMod-Sel`
  - `:BState`, internally a forward link to `{SCE}:BState-Sel`
  - `:BDestination`, internally a forward link to `{SCE}:BDest-Sel`
- New Timing Tables
  - `:TTData` long waveform of data for the new added TimingTable
  - `:TTHeader` string waveform of header definition for the new added TimingTable
  - :TTFileName as string for the file name to save
- Current Timing tables:
  - `:TTToLoad`, forward to `{SCE}:ScTable-SP`
  - `:TTSource`, forward to `{SCE}:ScTableDir-SP`
- Python generator for the EPICS db files (based on `reftabs` yaml files)
- Basic SCE mockup for the forwarded fields

